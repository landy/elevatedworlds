﻿namespace ElevatedWorlds
  
  module MapModule =
    let optionMap f xOpt =
      match xOpt with
      | None -> None
      | Some x -> Some (f x)

    let mapIn f =
      let inner xOpt =
        match xOpt with
        | None -> None
        | Some x -> Some (f x)
      inner  

    let foo = optionMap (+)
    let bar = mapIn (+)

  module ReturnModule =
    let returnOption x = Some x

  module OptionApplyModule =
    let (<!>) = Option.map
    let apply fOpt xOpt =
      match fOpt,xOpt with
      | Some f,Some x -> Some(f x)
      | _ -> None
    let (<*>) = apply
    let lift2 f x y =
      f <!> x <*> y
    let lift2Norm f x y =
      let foo = Option.map f x
      let bar = apply foo y
      bar
    let lift f x =
      f <!> x

    let lift3 f x y z =
      f <!> x <*> y <*> z

    let lift2Fn f =
      let inner1 x =
        let inner2 y =
          f <!> x <*> y
        inner2
      inner1        

    let add1 x = x + 1
    let add x y = x + y
    let add3 x y z = x + y + z
    let addStr (x: int) (y: int) = sprintf "%i %i" x y
    let add1Opt = apply (Some add1)
    let addTest = apply (Some add1) (Some 2)
    let addOpt = apply (apply (Some add) (Some 2))
    
    let add1Lifted = apply (Some add1)
    let addLifted = apply (Some add)
    let addLifted2 = lift2 add

    let result = (Some add) <*> (Some 2) <*> (Some 3)
    let res2 = (<*>) (Some add) (Some 2)
    let res3 = apply res2 (Some 3)

    let addTest1 = ReturnModule.returnOption add1
    let mapTest1 = apply addTest1
    let foo = Option.map add1

    
    let res4 = add <!> (Some 2) <*> (Some 3)
    let bar = Option.map add (Some 2)
    let baz = Option.map add
    let baz2 = MapModule.optionMap add
    let bazStr = MapModule.optionMap addStr

  module ListApplyModule =
    let apply fList xList =
      [ for f in fList do
        for x in xList do
          yield f x
      ]